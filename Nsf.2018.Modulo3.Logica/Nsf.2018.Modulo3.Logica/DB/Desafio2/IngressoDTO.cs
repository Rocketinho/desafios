﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.Logica.DB.Desafio2
{
    class IngressoDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public DateTime Data { get; set; }
        public decimal Preco { get; set; }
        public string Tipo { get; set; }
        public string Lugar { get; set; }
        public bool Reservado { get; set; }
    }
}
