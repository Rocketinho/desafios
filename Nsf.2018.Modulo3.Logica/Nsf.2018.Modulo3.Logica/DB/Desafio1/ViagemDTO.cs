﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nsf._2018.Modulo3.Logica.DB.Desafio1
{
    class ViagemDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public decimal ValorPassagemArea { get; set; }
        public decimal ValorHotelAdulto { get; set; }
        public decimal ValorHotelCrianca { get; set; }
    }
}
