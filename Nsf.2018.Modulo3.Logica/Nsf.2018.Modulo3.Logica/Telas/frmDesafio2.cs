﻿using Nsf._2018.Modulo3.Logica.DB.Desafio2;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.Logica.Telas
{
    public partial class frmDesafio2 : Form
    {
        public frmDesafio2()
        {
            InitializeComponent();

            
        }

        private void btnIistar_Click(object sender, EventArgs e)
        {
            IngressoBusiness bus = new IngressoBusiness();
            List<IngressoDTO> lista = bus.Listar();

            dgvIngressos.AutoGenerateColumns = false;
            dgvIngressos.DataSource = lista;

            foreach (DataGridViewRow item in dgvIngressos.SelectedRows)
            {
                
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
                IngressoDTO dto = dgvIngressos.CurrentRow.DataBoundItem as IngressoDTO;
                
                if(dto.Reservado == false)
                {
                    dto.Reservado = true;
                    MessageBox.Show("Seu ingresso foi resevado", "Show", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    IngressoBusiness busi = new IngressoBusiness();
                    busi.Atualizar(dto);
                }

                else if(dto.Reservado == true)
                {
                    MessageBox.Show("Ingresso já foi reservado", "Show", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            

        }

        private void button3_Click(object sender, EventArgs e)
        {
            IngressoDTO dto = dgvIngressos.CurrentRow.DataBoundItem as IngressoDTO;

            if (dto.Reservado == true)
            {
                dto.Reservado = false;
                IngressoBusiness busi = new IngressoBusiness();
                busi.Atualizar(dto);
                MessageBox.Show("Ingresso cancelado", "Show", MessageBoxButtons.OK, MessageBoxIcon.Information);
                
            }

            else if (dto.Reservado == false)
            {
                MessageBox.Show("Ingresso não está reservado");
            }
        }

        private void frmDesafio2_Load(object sender, EventArgs e)
        {

        }
    }
}
