﻿using Nsf._2018.Modulo3.Logica.DB.Desafio1;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nsf._2018.Modulo3.Logica.Telas
{
    public partial class frmDesafio1 : Form
    {
        public frmDesafio1()
        {
            InitializeComponent();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            ViagemBusiness bus = new ViagemBusiness();
            List<ViagemDTO> lista = bus.Listar();

            dgvViagem.AutoGenerateColumns = false;
            dgvViagem.DataSource = lista;
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            ViagemDTO dto = dgvViagem.CurrentRow.DataBoundItem as ViagemDTO;

            TimeSpan dias1 = dtpVolta.Value - dtpIda.Value;

         

            int dias = Convert.ToInt32(dias1.TotalDays);

            decimal valordiariaAdulto = dto.ValorHotelAdulto *  Convert.ToInt32(txtAdulto.Text) * dias;
            decimal valordiariaCrianca= dto.ValorHotelCrianca * Convert.ToInt32(txtCrianca.Text) * dias;
            decimal ValorTotal = valordiariaAdulto + valordiariaCrianca + dto.ValorPassagemArea;

            lblTotal.Text = ValorTotal.ToString();



        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }

        private void toolTip2_Popup(object sender, PopupEventArgs e)
        {

        }

        private void frmDesafio1_Load(object sender, EventArgs e)
        {

        }
    }
}
